from MVC.view import View
from MVC.model import Model
from kivy.core.window import Window


class Controller():

    def __init__(self):
        Window.minimum_width = 200
        Window.minimum_height = 200
        Window.size = (400, 400)
        self.view = View(controller=self, size_hint=(1, 1))
        self.model = Model(self.view)
        Window.bind(on_show=self.view.current_layout.resize_widgets,
                    on_resize=self.view.current_layout.resize_widgets,
                    on_maximize=self.view.current_layout.resize_widgets,
                    on_restore=self.view.current_layout.resize_widgets)

    def screen(self):
        return self.view

    def on_button_press(self, instance):
        self.model.interprate_press(instance)
