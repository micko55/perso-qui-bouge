import json
from MVC.user import User


class Model():
    def __init__(self, view):
        self.view = view
        self._stored_users = []
        self._user = None

    @property
    def stored_users(self):
        return self._stored_users

    @property
    def user(self):
        return self._user

    @stored_users.setter
    def stored_users(self, value):
        self._stored_users = value

    @user.setter
    def user(self, value):
        self._user = value

    def interprate_press(self, button):
        if button.text == "Login":
            self._login(self.view.current_layout, self.view.current_layout.textInputs)
        elif button.text == "Register":
            self._register_user(self.view.current_layout.textInputs)
        elif button.text == "Back":
            self._goto_main_screen()
        elif "^" or "v" or "<" or ">" in button.text:
            self._move_character(button.text)

    def _move_character(self, text):
        personnage = self.view.current_layout.personnage
        if text == "<":
            personnage.move(-.1, 0)
        if text == ">":
            personnage.move(.1, 0)
        if text == "^":
            personnage.move(0, .1)
        if text == "v":
            personnage.move(0, -.1)
        self.view.current_layout.do_layout()

    def _goto_main_screen(self):
        layout = self.view.current_layout
        self._save_character(layout.personnage)
        [layout.remove_widget(w) for w in layout.buttons]
        layout.remove_widget(layout.personnage)
        self.view.change_window("MainScreen")
        return

    def _login(self, layout, view_inputs):
        for vi in view_inputs:
            if not vi.text:
                layout.labels[-1].text = "Could not login."
                return
            self._get_users()
            for user in self.stored_users:
                if [user.surname, user.name, user.age] == [view_inputs[0].text, view_inputs[1].text,
                                                           view_inputs[2].text]:
                    [layout.remove_widget(w) for w in layout.labels]
                    [layout.remove_widget(w) for w in layout.textInputs]
                    [layout.remove_widget(w) for w in layout.buttons]
                    self.view.change_window("CharacterScreen")
                    self.user = user
                    self._create_personnage()
                    return
        layout.labels[-1].text = "Could not login."

    def _create_personnage(self):
        layout = self.view.current_layout
        from personnage import Personnage
        personnage = Personnage(source="images/personnage.png", pos_hint={"center_x": .5, "center_y": .5},
                                size_hint=(.2, .2), allow_stretch=True)
        layout.add_widget(personnage, index=10)
        personnage.bind(on_touch_down=self.reset_player)
        self._load_persoInfo(personnage)
        layout.personnage = personnage

    def reset_player(self, instance=None, touch=None):
        if touch is not None and instance.collide_point(*touch.pos):
            instance.pos_hint["center_x"] = .5
            instance.pos_hint["center_y"] = .5
            instance.diffX = 0
            instance.diffY = 0
            instance.move_to(instance.pos_hint["center_x"], instance.pos_hint["center_y"])
            self.view.current_layout.do_layout()

    def _load_persoInfo(self, perso):
        try:
            with open("perso_pos.json", "r") as f:
                jsonDict = json.load(f)
                if str(self.user.id) in jsonDict.keys():
                    infos = jsonDict[str(self.user.id)]
                    perso.move_to(infos["center_x"], infos["center_y"])
                    return
        except FileNotFoundError:
            pass
        perso.move_to(.5, .5)

    def _save_character(self, perso):
        jsonDict = self._get_characters()
        with open("perso_pos.json", "w") as f:
            jsonDict[str(self.user.id)] = perso.pos_hint
            json.dump(jsonDict, f, indent=4, ensure_ascii=False)

    def _get_characters(self):
        jsonDict = {}
        try:
            with open("perso_pos.json", "r") as f:
                jsonDict = json.load(f)
        except FileNotFoundError:
            pass
        return jsonDict

    def _register_user(self, view_inputs):
        self._get_users()
        with open("users.json", "w") as f:
            registerInfo = {}
            if self.stored_users:
                for user in self.stored_users:
                    registerInfo[str(user.id)] = {"Nom": user.surname, "Prénom": user.name, "Âge": user.age}
            user = self._add_stored_user(view_inputs)
            if isinstance(user, User):
                registerInfo[str(user.id)] = {"Nom": user.surname, "Prénom": user.name, "Âge": user.age}
                self.view.current_layout.labels[-1].text = "Registered!"
            if registerInfo:
                json.dump(registerInfo, f, indent=4, ensure_ascii=False)

    def _get_users(self):
        self.stored_users = []
        try:
            with open("users.json", 'r') as f:
                jsonDict = json.load(f)
                for key, value in jsonDict.items():
                    self._add_stored_user([value["Nom"], value["Prénom"], value["Âge"]])
        except FileNotFoundError:
            pass

    def _add_stored_user(self, view_inputs):
        i = len(self.stored_users)
        if isinstance(view_inputs[0], str):
            vi = (view_inputs[0], view_inputs[1], view_inputs[2])
        else:
            vi = (view_inputs[0].text, view_inputs[1].text, view_inputs[2].text)
        for user in self.stored_users:
            if user.surname == vi[0] and user.name == vi[1] and user.age == vi[2]:
                self.view.current_layout.labels[-1].text = "Already Registered"
                return None
        user = User(i, vi[0], vi[1], vi[2])
        self.stored_users.append(user)
        return user
