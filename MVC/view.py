from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput


class View(FloatLayout):

    def __init__(self, controller, **kwargs):
        super().__init__(**kwargs)
        self._controller = controller
        self._current_layout = None
        self.change_window("MainScreen")

    @property
    def controller(self):
        return self._controller

    @property
    def current_layout(self):
        return self._current_layout

    @controller.setter
    def controller(self, value):
        self._controller = value

    @current_layout.setter
    def current_layout(self, value):
        self._current_layout = value

    def change_window(self, window):
        if self.current_layout is not None:
            self.remove_widget(self.current_layout)
        if window == "MainScreen":
            self.current_layout = MainScreen(controller=self.controller, size_hint=(1, 1), pos_hint={"x": 0, "y": 0})
        elif window == "CharacterScreen":
            self.current_layout = CharacterScreen(controller=self.controller, size_hint=(1, 1),
                                                  pos_hint={"x": 0, "y": 0})
        self.add_widget(self.current_layout)
        Window.bind(on_show=self.current_layout.resize_widgets,
                    on_resize=self.current_layout.resize_widgets,
                    on_maximize=self.current_layout.resize_widgets,
                    on_restore=self.current_layout.resize_widgets)


class MainScreen(FloatLayout):
    def __init__(self, controller, **kwargs):
        super().__init__(**kwargs)
        self._controller = controller
        self._labels = self._create_labels()
        self._textInputs = self._create_textInputs()
        self._buttons = self._create_buttons()
        self.resize_widgets(Window, Window.width, Window.height)

    @property
    def controller(self):
        return self._controller

    @property
    def labels(self):
        return self._labels

    @property
    def textInputs(self):
        return self._textInputs

    @property
    def buttons(self):
        return self._buttons

    @controller.setter
    def controller(self, value):
        self._controller = value

    def _create_labels(self):
        labels = [
            self._create_label("Nom", 0, .8, .5, .2),
            self._create_label("Prénom", 0, .6, .5, .2),
            self._create_label("Âge", 0, .4, .5, .2),
            self._create_label("", 0, .2, 1, .2, halign="center", padding_x=0)
        ]
        return labels.copy()

    def _create_label(self, name, x, y, width, height, halign="right", padding_x=10):
        h = height * Window.height
        w = width * Window.width
        label = Label(text=name, pos_hint={"x": x, "y": y}, size_hint=(width, height),
                      color=(1, 1, 1, 1), text_size=(w, h), valign='center', halign=halign, padding_x=padding_x)
        self.add_widget(label)
        return label

    def _create_textInputs(self):
        textInputs = [
            self._create_textInput(.5, .8, .5, .2),
            self._create_textInput(.5, .6, .5, .2),
            self._create_textInput(.5, .4, .5, .2, intOnly=True)
        ]
        return textInputs.copy()

    def _create_textInput(self, x, y, width, height, intOnly=False):
        if intOnly:
            textInput = TextInput(pos_hint={"x": x, "y": y}, size_hint=(width, height), input_filter='int')
        else:
            textInput = TextInput(pos_hint={"x": x, "y": y}, size_hint=(width, height))
        self.add_widget(textInput)
        return textInput

    def _create_buttons(self):
        buttons = [
            self._create_button("Login", 0, 0, .5, .2),
            self._create_button("Register", .5, 0, .5, .2)
        ]
        return buttons.copy()

    def _create_button(self, name, x, y, width, height):
        cb = CustomButton(text=name, pos_hint={"x": x, "y": y}, size_hint=(width, height), color=(0, 0, 0, 1))
        cb.bind(on_press=self.controller.on_button_press)
        self.add_widget(cb)
        return cb

    def resize_widgets(self, window=None, width=None, height=None):
        for label in self._labels:
            label.text_size = (width * .5, height * .5)


class CharacterScreen(FloatLayout):
    def __init__(self, controller, **kwargs):
        super().__init__(**kwargs)
        self._controller = controller
        self._personnage = None
        self._buttons = self._create_buttons()
        self.resize_widgets()

    @property
    def controller(self):
        return self._controller

    @property
    def personnage(self):
        return self._personnage

    @property
    def buttons(self):
        return self._buttons

    @controller.setter
    def controller(self, value):
        self._controller = value

    @personnage.setter
    def personnage(self, value):
        self._personnage = value

    @buttons.setter
    def buttons(self, value):
        self._buttons = value

    def _create_buttons(self):
        buttons = [
            self._create_button("^", .8, .1, .1, .1),
            self._create_button("<", .7, 0, .1, .1),
            self._create_button("v", .8, 0, .1, .1),
            self._create_button(">", .9, 0, .1, .1),
            self._create_button("Back", 0, 0, .2, .1)
        ]
        return buttons.copy()

    def _create_button(self, name, x, y, width, height):
        cb = CustomButton(text=name, pos_hint={"x": x, "y": y}, size_hint=(width, height), halign="center",
                          valign="center", color=(0, 0, 0, 1))
        cb.bind(on_press=self.controller.on_button_press)
        self.add_widget(cb, index=0)
        return cb

    def resize_widgets(self, window=None, width=None, height=None):
        pass


class CustomButton(Button):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.background_normal = 'atlas://images/defaulttheme/button'
