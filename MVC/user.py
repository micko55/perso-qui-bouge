class User():
    def __init__(self, i, surname, name, age):
        self._id = i
        self._surname = surname
        self._name = name
        self._age = age

    @property
    def id(self):
        return self._id

    @property
    def surname(self):
        return self._surname

    @property
    def name(self):
        return self._name

    @property
    def age(self):
        return self._age

    @surname.setter
    def surname(self, value):
        self._surname = value

    @name.setter
    def name(self, value):
        self._name = value

    @age.setter
    def age(self, value):
        self._age = value

    @id.setter
    def id(self, value):
        self._id = value
