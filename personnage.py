from kivy.uix.image import Image


class Personnage(Image):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._diffX = 0
        self._diffY = 0

    @property
    def diffX(self):
        return self._diffX

    @property
    def diffY(self):
        return self._diffY

    @diffX.setter
    def diffX(self, value):
        self._diffX = value

    @diffY.setter
    def diffY(self, value):
        self._diffY = value

    def move(self, x, y):
        self.diffX += x
        self.diffY += y
        self.move_to()

    def move_to(self, x=None, y=None):
        if x is not None:
            self.pos_hint = {"center_x": .5, "center_y": .5}
            self.diffX = x - .5
            self.diffY = y - .5
        if self.diffX < -.5:
            self.diffX = .5
        elif self.diffX > .5:
            self.diffX = -.5
        if self.diffY < -.5:
            self.diffY = .5
        elif self.diffY > .5:
            self.diffY = -.5
        self.pos_hint = {"center_x": .5 + self.diffX, "center_y": .5 + self.diffY}
